# Import required libraries
import copy
import datetime as dt
import math

import pandas as pd
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html

# Multi-dropdown options --

EEG_STATUSES = dict(
    AC='EEG 2000',  # cb
    AR='EGG 2004',  # cb
    CA='EGG 2009',  # cb
    DC='EGG 2014',  # cb
    DD='EGG 2017',  # cb
)

IND_TYPES = dict(
    BR='Wind',
    DH='Solar',
    THG='THG',
)

WELL_COLORS = dict(
    GD='#FFEDA0',  # gelb
    GE='#FA9FB5',  # rot dunkel
    GW='#A1D99B',  # grün
    IG='#67BD65',  # grün dunkel
    OD='#BFD3E6',  # blau hind
    OE='#B3DE69',  # grün
    OW='#FDBF6F',  # orange
    ST='#FC9272',  # rötlich
    BR='#D0D1E6',  # blau hind
    MB='#ABD9E9',  # türkis
    IW='#3690C0',  # blau dunkel
    LP='#F87A72',  # rot
    MS='#CA6BCC',  # lila
    Confidential='#DD3497',  # rot dunkel
    DH='#4EB3D3',  # türkis dunkel
    DS='#FFFF33',  # gelb
    DW='#FB9A99',  # rot
    MM='#A6D853',  # grün
    NL='#D4B9DA',  # hind lila
    OB='#AEB0B8',  # dunkel grau
    SG='#CCCCCC',  # grau
    TH='#EAE5D9',  # bech
    UN='#C29A84',  # dunkles bech
)


app = dash.Dash(__name__)
server = app.server

# Create controls #todo: eeg_status und ind_type kann beliebig ersetzt werden
eeg_status_options = [{'label': str(EEG_STATUSES[eeg_status]),
                       'value': str(eeg_status)}
                      for eeg_status in EEG_STATUSES]

ind_type_options = [{'label': str(IND_TYPES[ind_type]),
                     'value': str(ind_type)}
                    for ind_type in IND_TYPES]


# Create global chart template
mapbox_access_token = 'pk.eyJ1IjoiamFja2x1byIsImEiOiJjajNlcnh3MzEwMHZtMzNueGw3NWw5ZXF5In0.fk8k06T96Ml9CLGgKmk81w'

layout = dict(
    autosize=True,
    automargin=True,
    margin=dict(
        l=30,
        r=30,
        b=20,
        t=40
    ),
    hovermode="closest",
    plot_bgcolor="#F9F9F9",
    paper_bgcolor="#F9F9F9",
    legend=dict(font=dict(size=10), orientation='h'),
    title='Kartenübersicht',
    mapbox=dict(
        accesstoken=mapbox_access_token,
        style="light",
        center=dict(
            lon=-78.05,
            lat=42.54
        ),
        zoom=7,
    )
)

# Create app layout
app.layout = html.Div(
    [
        dcc.Store(id='aggregate_data'),
        html.Div(
            [
                html.Div(
                    [
                        html.H2(
                            'Validierung der Effektivität von nationaler Klimaschutzpolitik',

                        ),
                        html.H4(
                            'Mit KI Unterstützung',
                        )
                    ],

                    className='eight columns'
                ),
                html.Img(
                    src="https://www.destatis.de/SiteGlobals/Frontend/Images/logo.svg?__blob=normal&v=9",
                    className='two columns',
                ),
                html.A(
                    html.Button(
                        "DataSource",
                        id="learnMore"

                    ),
                    href="https://www.destatis.de/DE/Home/_inhalt.html",
                    className="two columns"
                )
            ],
            id="header",
            className='row',
        ),
        html.Div(
            [
                html.Div(
                    [
                        html.P(
                            'Auswahl des betrachteten Zeitraums:',
                            className="control_label"
                        ),
                        dcc.RangeSlider(
                            id='year_slider',
                            min=1990,
                            max=2020,
                            value=[1990, 2010],
                            className="dcc_control"
                        ),
                        html.P(
                            'Auswahl der politischen Entscheidung:',
                            className="control_label"
                        ),
                        dcc.RadioItems(
                            id='eeg_status_selector',
                            options=[
                                {'label': 'Alle ', 'value': 'all'},
                                {'label': 'auswählen ', 'value': 'custom'}
                            ],
                            value='active',
                            labelStyle={'display': 'inline-block'},
                            className="dcc_control"
                        ),
                        dcc.Dropdown(
                            id='eeg_statuses',
                            options=eeg_status_options,
                            multi=True,
                            value=list(EEG_STATUSES.keys()),
                            className="dcc_control"
                        ),
                        html.P(
                            'Auswahl der Klimaschutzkategorie:',
                            className="control_label"
                        ),
                        dcc.RadioItems(
                            id='wind_type_selector',
                            options=[
                                {'label': 'Alle ', 'value': 'all'},
                                {'label': 'auswählen', 'value': 'custom'}
                            ],
                            value='productive',
                            labelStyle={'display': 'inline-block'},
                            className="dcc_control"
                        ),
                        dcc.Dropdown(
                            id='wind_types',
                            options=[
                                {'label': 'Wind', 'value': 'wind'},
                                {'label': 'Solar', 'value': 'solar'},
                                {'label': 'THG', 'value': 'thg'}
                            ],
                            # multi=True,
                            multi=False,
                            value='wind',
                            className="dcc_control"
                        ),
                    ],
                    className="pretty_container four columns"
                ),
                html.Div(
                    [
                        html.Div(
                            id="infoContainer",
                            className="row"
                        ),
                        html.Div(
                            [
                                dcc.Graph(
                                    id='count_graph',
                                )
                            ],
                            id="countGraphContainer",
                            className="pretty_container"
                        )
                    ],
                    id="rightCol",
                    className="eight columns"
                )
            ],
            className="row"
        ),
        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id='main_graph')
                    ],
                    className='pretty_container eight columns',
                ),
                html.Div(
                    [
                        dcc.Graph(id='individual_graph')
                    ],
                    className='pretty_container four columns',
                ),
            ],
            className='row'
        ),
        html.Div(
            [
                html.Div(
                    """ [
                        dcc.Graph(id='pie_graph')
                    ],
                    className='pretty_container seven columns', """
                ),
                html.Div(
                    [
                        dcc.Graph(id='aggregate_graph')
                    ],
                    className='pretty_container five columns',
                ),
            ],
            className='row'
        ),
    ],
    id="mainContainer",
    style={
        "display": "flex",
        "flex-direction": "column"
    }
)


# Helper functions
def human_format(num):

    magnitude = int(math.log(num, 1000))
    mantissa = str(int(num / (1000**magnitude)))
    return mantissa + ['', 'K', 'M', 'G', 'T', 'P'][magnitude]


def filter_dataframe(df, eeg_statuses, wind_types, year_slider):
    dff = df[df['Wind_Status'].isin(eeg_statuses)
             & df['Wind_Type'].isin(wind_types)
             & (df['Date_Wind_Completed'] > dt.datetime(year_slider[0], 1, 1))
             & (df['Date_Wind_Completed'] < dt.datetime(year_slider[1], 1, 1))]
    return dff


def fetch_aggregate(selected, year_slider):

    index = list(range(max(year_slider[0], 1985), 2016))
    data1 = []
    data2 = []
    water = []

    for year in index:
        count_gas = 0
        count_oil = 0
        count_water = 0
        for api in selected:
            try:
                count_gas += points[api][year]['Gas Produced, MCF']
            except:
                pass
            try:
                count_oil += points[api][year]['Oil Produced, bbl']
            except:
                pass
            try:
                count_water += points[api][year]['Water Produced, bbl']
            except:
                pass
        data1.append(count_gas)
        data2.append(count_oil)
        water.append(count_water)

    return index, data1, data2, water


# Create callbacks
@app.callback(Output('aggregate_data', 'data'),
              [Input('eeg_statuses', 'value'),
               Input('wind_types', 'value'),
               Input('year_slider', 'value')])
def update_production_text(eeg_statuses, wind_types, year_slider):

    dff = filter_dataframe(df, eeg_statuses, wind_types, year_slider)
    selected = dff['API_WindNo'].values
    index, data1, data2, water = fetch_aggregate(selected, year_slider)
    return [human_format(sum(data1)), human_format(sum(data2)), human_format(sum(water))]

# Radio -> multi


@app.callback(Output('eeg_statuses', 'value'),
              [Input('eeg_status_selector', 'value')])
def display_status(selector):
    if selector == 'all':
        return list(EEG_STATUSES.keys())
    elif selector == 'active':
        return ['AC']
    else:
        return []


# Radio -> multi
@app.callback(Output('wind_types', 'value'),
              [Input('wind_type_selector', 'value')])
def display_type(selector):
    if selector == 'all':
        return list(IND_TYPES.keys())
    elif selector == 'productive':
        return ['GD', 'GE', 'GW', 'IG', 'IW', 'OD', 'OE', 'OW']
    else:
        return []


# Slider -> count graph
@app.callback(Output('year_slider', 'value'),
              [Input('count_graph', 'selectedData')])
def update_year_slider(count_graph_selected):

    if count_graph_selected is None:
        return [1990, 2020]
    else:
        nums = []
        for point in count_graph_selected['points']:
            nums.append(int(point['pointNumber']))

        return [min(nums) + 1960, max(nums) + 1961]


# Selectors -> wind text
@app.callback(Output('wind_text', 'children'),
              [Input('eeg_statuses', 'value'),
               Input('wind_types', 'value'),
               Input('year_slider', 'value')])
def update_wind_text(eeg_statuses, wind_types, year_slider):

    dff = filter_dataframe(df, eeg_statuses, wind_types, year_slider)
    return dff.shape[0]


# Selectors -> main graph

@app.callback(Output('main_graph', 'figure'),
              Input('wind_types', 'value'))
def make_main_figure(wind_types):
    layout_individual = copy.deepcopy(layout)

    df = pd.read_csv('data/ResultData_wind.csv')
    index = df['jahr']
    data1 = df['wind_kapazitaet']
    data2 = df['Vorhersage']

    if wind_types == 'wind':
        df = pd.read_csv('data/ResultData_wind.csv')
        index = df['jahr']
        data1 = df['wind_kapazitaet']
        data2 = df['Vorhersage']

    if wind_types == 'thg':
        df = pd.read_csv('data/ResultData_thg.csv')
        index = df['jahr']
        data1 = df['treibhausgase']
        data2 = df['Vorhersage']

    if wind_types == 'solar':
        df = pd.read_csv('data/ResultData_solar.csv')
        index = df['jahr']
        data1 = df['solar_kapazitaet']
        data2 = df['Vorhersage']

    data = [
        dict(
            type='scatter',
            mode='lines+markers',
            name='Real',
            x=index,
            y=data1,
            line=dict(
                shape="spline",
                smoothing=2,
                width=1,
                color='#fac1b7'
            ),
            marker=dict(symbol='diamond-open')
        ),
        dict(
            type='scatter',
            mode='lines+markers',
            name='Prognose',
            x=index,
            y=data2,
            line=dict(
                shape="spline",
                smoothing=2,
                width=1,
                color='#a9bb95'
            ),
            marker=dict(symbol='diamond-open')
        )
    ]
    layout_individual['title'] = 'Indikatoren'
    layout_individual['showlegend'] = False
    layout_individual['autosize'] = True
    layout_individual['title'] = 'Indikatoren'
    figure = dict(data=data, layout=layout_individual)
    return figure


@app.callback(Output('count_graph', 'figure'),
              [Input('year_slider', 'value'),
               Input('wind_types', 'value')])
def make_count_figure(year_slider, wind_types):

    layout_count = copy.deepcopy(layout)

    if wind_types == 'wind':
        data = pd.read_csv('data/ResultData_wind.csv')
        data['Diff'] = data['wind_kapazitaet'] - data['Vorhersage']
    if wind_types == 'solar':
        data = pd.read_csv('data/ResultData_solar.csv')
        data['Diff'] = data['solar_kapazitaet'] - data['Vorhersage']
    if wind_types == 'thg':
        data = pd.read_csv('data/ResultData_thg.csv')
        data['Diff'] = data['treibhausgase'] - data['Vorhersage']

    index = data['jahr']

    colors = []
    for i in range(1990, 2021):
        if i >= int(year_slider[0]) and i < int(year_slider[1]):
            colors.append('rgb(123, 199, 255)')
        else:
            colors.append('rgba(123, 199, 255, 0.2)')

    data = [
        dict(
            type='bar',
            x=index,
            y=data['Diff'],
            name='All Winds',
            marker=dict(
                color=colors
            ),
        ),
    ]

    layout_count['title'] = 'Kumulative Darstindung der Indikatoren'
    layout_count['dragmode'] = 'select'
    layout_count['showlegend'] = False
    layout_count['autosize'] = True

    figure = dict(data=data, layout=layout_count)
    return figure


# Main
if __name__ == '__main__':
    app.server.run(debug=True, threaded=True)
